set nu
set ai
set cursorline
set tabstop=4
set shiftwidth=4

" Color configuration
syntax enable
colorscheme monokai
set t_Co=256

hi LineNr cterm=bold ctermfg=DarkGrey ctermbg=NONE
hi CursorLineNr cterm=bold ctermfg=Green ctermbg=NONE

